﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
namespace test_Manager
{
    class Program
    {
        [Serializable]
        class TestPackage
        {
            public int x;
            public int y;
           public TestPackage()
            {
                x = 1;
                y = 2;
            }
           public void SerilizationExample()
            {
                TestPackage t = new TestPackage();
                Console.WriteLine("{0} , {1}", t.x, t.y);
                //serialization
                MemoryStream memstream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(memstream, t);
                byte[] data = memstream.ToArray();

                Console.WriteLine("size packet = {0}", data.Length);

                //deserialization 
                MemoryStream memstream1 = new MemoryStream(data);
                BinaryFormatter formatter1 = new BinaryFormatter();
                TestPackage t2 = (TestPackage)formatter1.Deserialize(memstream1);
                Console.WriteLine("{0} , {1}", t2.x, t2.y);
            }
            public void SwitchAndReadExample()
            {
                while (true)
                {
                    char c = Console.ReadKey(true).KeyChar;
                    switch (c)
                    {
                        case 'w':
                            Console.Write('w');
                            break;
                        case 's':
                            Console.Write('s');
                            break;
                        case 'a':
                            Console.Write('a');
                            break;
                        case 'd':
                            Console.Write('d');
                            break;
                    }

                }
            }

        }
        static void Main(string[] args)
        {
            int i = 0;
            // Random r = new Random();
            //while (i != 5)
            // {
            //    Console.WriteLine(r.Next(10));
            //    i++;
            //}
            Console.WriteLine(0.1 + 0.2 == 0.3);
            Console.ReadLine();
        }
    }
}
