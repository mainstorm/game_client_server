﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
namespace Client
{
    class Program
    {
        const int Port = 11000;
        static int i = 8;
        static int client_1 = 1;
        static int client_2 = 1;
        static List<Socket> clients = new List<Socket>();

        static char[,] FIELD;
        static int x = 20;
        static int y = 20;
        static void Init_field()
        {

            FIELD = new char[x, y];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    if (j == 0 || j == x - 1 || i == 0 || i == y - 1)
                        FIELD[i, j] = '#';
                }
            }
        }
        static void Print_field()
        {
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                    Console.Write(FIELD[i, j]);
                Console.Write('\n');
            }
        }
        static void Clear_field()
        {
            for (int i = 1; i < y - 1; i++)
                for (int j = 1; j < x - 1; j++)
                    FIELD[i, j] = ' ';
        }

        static void Main(string[] args)
        {
            try
            {
                Run();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            Console.ReadKey();
        }

        public static string receive_string(Socket handler)
        {
            byte[] buffer_for_size = new byte[4];
            handler.Receive(buffer_for_size, 0, 4, SocketFlags.None);
            int bufferSize = BitConverter.ToInt32(buffer_for_size, 0);
            byte[] buffer = new byte[bufferSize];
            int numberRecievedBytes = handler.Receive(buffer, 0, bufferSize, SocketFlags.None);
            var result = Encoding.ASCII.GetString(buffer, 0, bufferSize);
            return result;
        }
        public static void send_sring(Socket socket, string s)
        {
            byte[] buffer_size = BitConverter.GetBytes(s.Length);
            socket.Send(buffer_size, 0, buffer_size.Length, SocketFlags.None);

            byte[] buffer = Encoding.ASCII.GetBytes(s);
            socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
        }

        public static void send_int(Socket socket, int s)
        {
            byte[] buffer = BitConverter.GetBytes(s);
            socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
        }

        public static int receive_int(Socket socket)
        {
            byte[] buffer = new byte[4];
            socket.Receive(buffer, 0, 4, SocketFlags.None);
            int data = BitConverter.ToInt32(buffer, 0);
            return data;
        }
//----------------------------------------------------server class
        public static void recieve_mail(object socket)
        {
            Socket handler = (Socket)socket;
            while (true)
            {
                    client_1 = receive_int(handler);
                    client_2 = receive_int(handler);

                FIELD[5, client_1] = '*';
                FIELD[7, client_2] = '*';
                Console.Clear();
                Print_field();
                Clear_field();
               
               
            }
        
        }
    
//----------------------------------------------------server class 
        public static void connect_soccet()
        {
            string hostNmae = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostNmae);
            IPAddress address = hostEntry.AddressList.First();
            IPEndPoint endPoint = new IPEndPoint(address, Port);
            Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(endPoint);

            Init_field();
           
            if (receive_string(socket) == "start")
            new Thread(new ParameterizedThreadStart(recieve_mail)).Start(socket);

            Console.WriteLine("socket.Close()");

        }
        private static void Run()
        {   
                new Thread(new ThreadStart(connect_soccet)).Start();
        }
    }
}
