﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace async_server
{
    class Program
    {
        const int Port = 11000;
        const int Backlog = 1000;
        static List<Socket> clients = new List<Socket>();
        static List<Point> clients_point = new List<Point>();
        static Field field = new Field();

        public static async Task AsyncMain(Socket socket)
        {
            bool res = true;
            
            while (res)
            {
                Socket client = await AsyncAccept(socket);
                
                clients.Add(client);
                clients_point.Add(new Player().RandomPoint());
                field.Add_player(clients_point[clients.IndexOf(client)]);

                CommunicationWithClient(client);
            }
        }

        static async Task<char> AsyncReceive(Socket socket)
        {
            byte[] buffer = new byte[1];
            await Task.Run(() =>
            {
                return socket.Receive(buffer, 0, 1, SocketFlags.None); 
            });
            var result = Encoding.ASCII.GetChars(buffer);
            
            return result[0];
        }
        static async Task<Socket> AsyncAccept(Socket socket)
        {
            return await Task.Run(() =>
            {
                return socket.Accept();
            });
        }
        static async Task AsyncSend(Socket socket,string massage)
        {
           await Task.Run(() =>
             {
                 byte[] buffer = Encoding.ASCII.GetBytes(massage);
                 socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
             });
            
        }
        static async Task CommunicationWithClient(Socket client)
        {
            char massage;
            Point point_user = new Point();
            while (true)
            {
                massage = await AsyncReceive(client);
                //нужен ли лок?
                switch (massage)
                {
                    case 'w':
                        point_user = new Player(clients_point[clients.IndexOf(client)]).buttonUp();
                        break;
                    case 's':
                        point_user = new Player(clients_point[clients.IndexOf(client)]).buttonDown();
                        break;
                    case 'a':
                        point_user = new Player(clients_point[clients.IndexOf(client)]).buttonLeft();
                        break;
                    case 'd':
                        point_user = new Player(clients_point[clients.IndexOf(client)]).buttonRight();
                        break;
                }

                clients_point[clients.IndexOf(client)] = point_user;
                AddPlayer();
                Console.Clear();
                field.Print_field(); 
                Console.WriteLine(massage);
            }
        }

        static void AddPlayer()
        {
            field.Clear_field();
            foreach (Point point in clients_point)
                field.Add_player(point);
        }

        /* bacup
        static async Task<string> AsyncReceive(Socket socket)
    {
        byte[] buffer = new byte[128];
        await Task.Run(() =>
        {
            return socket.Receive(buffer, 0, 127, SocketFlags.None);
        });
        var result = Encoding.ASCII.GetString(buffer, 0, 127);
        return result;
    }
    static async Task CommunicationWithClient(Socket client)
    {
        string massage;
        while (true)
        {
            massage = await AsyncReceive(client);

            Console.WriteLine(massage);
            foreach (Socket cl in clients)
                await AsyncSend(cl, massage);


        }*/

        static void Main(string[] args)
        {
            string hostNmae = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostNmae);
            IPAddress address = hostEntry.AddressList.First();
            IPEndPoint endPoint = new IPEndPoint(address, Port);
            Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(endPoint);
            socket.Listen(Backlog);
            AsyncMain(socket).GetAwaiter().GetResult();
        }
    }
}
