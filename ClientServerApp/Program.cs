﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace ClientServerApp
{
    class Program
    {
        const int Port = 11000;
        const int Backlog = 1000;
        static int client_1 = 1;
        static int client_2 = 1;

        static List<Socket> clients = new List<Socket>();
        static void Main(string[] args)
        {
            try
            {
                Run();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            Console.ReadKey();
        }

        public static string receive_string(Socket handler)
        {
            byte[] buffer_for_size = new byte[4];
            handler.Receive(buffer_for_size, 0, 4, SocketFlags.None);
            int bufferSize = BitConverter.ToInt32(buffer_for_size, 0);

            byte[] buffer = new byte[bufferSize];
            int numberRecievedBytes = handler.Receive(buffer, 0, bufferSize, SocketFlags.None);
            var result = Encoding.ASCII.GetString(buffer, 0, bufferSize);
            return result;
        }
        public static void send_sring(Socket socket, string s)
        {
            byte[] buffer_size = BitConverter.GetBytes(s.Length);
            socket.Send(buffer_size, 0, buffer_size.Length, SocketFlags.None);

            byte[] buffer = Encoding.ASCII.GetBytes(s);
            socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
        }

        public static void send_int(Socket socket, int s)
        {
            byte[] buffer = BitConverter.GetBytes(s);
            socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
        }

        public static int receive_int(Socket socket)
        {
            byte[] buffer = new byte[4];
            try
            {
                socket.Receive(buffer, 0, 4, SocketFlags.None);
            }
            catch (Exception e)
            {
                Console.WriteLine("its me mario");
            }
            int data = BitConverter.ToInt32(buffer, 0);
            return data;
        }

        public static void recieve_mail(object socket)
        {
            Socket handler = (Socket)socket;
            while (true)
            {

                if (clients.IndexOf(handler) == 0)
                    client_1 = receive_int(handler);
                else client_2 = receive_int(handler);
            }
        }
        public static  async Task  send_mail(object socket)
        {
            try
            {
               await Task.Run(() =>
                { 
                    Socket handler = (Socket)socket;
                while (true)
                {
                    Thread.Sleep(1000);

                    if (clients.IndexOf(handler) == 0)
                    {
                        client_1 = client_1 != 17 ? client_1 + 1 : 17;
                        send_int(handler, client_1);
                        send_int(handler, client_2);
                    }
                    else
                    {
                        client_2 = client_2 != 17 ? client_2 + 1 : 17;
                        send_int(handler, client_2);
                        send_int(handler, client_1);
                    }
                }
            });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



        public static void  acc(object socket)
        {
            
            while (true)
            {
                Socket handler = ((Socket)socket).Accept();
                clients.Add(handler);
                if (clients.Count == 2)
                {
                    Task allTasks = null;

                    send_sring(clients[0], "start");
                    send_sring(clients[1], "start");
                    Task t1 =  send_mail(clients[0]);
                    Task t2 =  send_mail(clients[1]);
                    allTasks = Task.WhenAll(t1, t2);
                      
                    
                    //new Thread(new ParameterizedThreadStart(send_mail)).Start(clients[0]);
                    //new Thread(new ParameterizedThreadStart(send_mail)).Start(clients[1]);
                    break;
                }
                    
            }

        }

        private static void Run()
        {
            string      hostNmae    = Dns.GetHostName();
            IPHostEntry hostEntry   = Dns.GetHostEntry(hostNmae);
            IPAddress   address     = hostEntry.AddressList.First();
            IPEndPoint  endPoint    = new IPEndPoint(address, Port);
            Socket      socket      = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                  socket.Bind(endPoint);
                  socket.Listen(Backlog);
            new Thread(new ParameterizedThreadStart(acc)).Start(socket);
        }

    }
}
