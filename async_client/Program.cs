﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace async_client
{
    class Program
    {
        const int Port = 11000;
        static async Task<string> AsyncReceive(Socket socket)
        {
            byte[] buffer = new byte[128];
            await Task.Run(() =>
            {
                return socket.Receive(buffer, 0, 127, SocketFlags.None);
            });
            var result = Encoding.ASCII.GetString(buffer, 0, 127);
            return result;
        }
        static async Task AsyncSend(Socket socket, string massage)
        {
            await Task.Run(() =>
            {
                byte[] buffer = Encoding.ASCII.GetBytes(massage);
                socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
            });

        }
        static async Task AsyncSendClient(Socket socket)
        {
            while (true)
                await AsyncSend(socket, Console.ReadKey(true).KeyChar.ToString());
        }
        static async Task AsyncReceiveClient(Socket socket)
        {
            string massage;

            while (true)
            {
                massage = await AsyncReceive(socket);
                Console.WriteLine(massage);
            }
        }

        static void Main(string[] args)
        {
            string hostNmae = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostNmae);
            IPAddress address = hostEntry.AddressList.First();
            IPEndPoint endPoint = new IPEndPoint(address, Port);
            Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endPoint);

            //AsyncReceiveClient(socket);
            AsyncSendClient(socket).GetAwaiter().GetResult();
        }
    }
}
