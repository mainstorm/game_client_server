﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace async_and_await
{
    class Program
    {
        public int MainChange; 
        public static async Task  print_id_thread()
        {
            await Task.Delay(5000); 
            await Task.Run(() => Console.WriteLine( "ID5  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId + " " + Thread.CurrentThread.Name));
            //Thread.Sleep(100);
            throw new Exception();
            await Task.Delay(100);
        }
        public static async void print_thread()
        {
             Console.WriteLine("ID6  синхронного  потока " + Thread.CurrentThread.ManagedThreadId);
        }

        public static async Task print_i()
        {
            await Task.Run(() =>
            {
                while (true)
                {
                    Console.WriteLine("ID  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId);
                    Thread.Sleep(7000);
                    Console.WriteLine("iiiiiiiiiiiii");
                }
            });
        }


        public static  void print_id_thread_run()
        {
            Console.WriteLine("IDrun  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId + " " + Thread.CurrentThread.Name);
            Thread.Sleep(100);
        }
        
        public static async Task print_b()
        {
            await Task.Run(() =>
             {
                 while (true)
                 {
                     Console.WriteLine("ID  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId);
                     Thread.Sleep(7000);
                     Console.WriteLine("bbbbbbbbbbbbbb");
                 }
             }); 
        }
        public static Task <int> bum()
        {
            Thread.Sleep(100);
            return  Task.Run(() => { Thread.CurrentThread.Name = "fox"; Console.WriteLine("IDbum  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId + " " + Thread.CurrentThread.Name); return 5; });
            
        }

        public static Task<int> bom()
        {
            return Task.Run(() => { Console.WriteLine("IDbom  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId + " " + Thread.CurrentThread.Name); return 6; });
        }


        public static async  void print_id_thread_async()
        {
            try
            {
                await print_id_thread();
            }
            catch (Exception e)
            {
                Console.WriteLine("erooor");
            }

        } 



        public static async void print()
        {
            //Action s,p;
            //s = print_i;
            //p = print_b;
            // Task.Run(s);
            //Task.Run(p);
            Console.WriteLine("========Аинхронный блок кода========");
            //Task t1 = print_b();
            //Task t2 = print_i();
            //Task all = Task.WhenAll(t1,t2);
           
            int f = await bum();
            Console.WriteLine(f + "ko-ko");
            await bom();
            // Console.WriteLine("ID4  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId);
            //Проблема в том что мы не дожидаемся Task 
          //  print_id_thread_async();


            Task.Run(() => print_id_thread_run());
            new Thread(new ThreadStart(print_thread)).Start();
            new Thread(new ThreadStart(print_thread)).Start();
            Task t3 = new Task(print_id_thread_run);
            Task t4 = new Task(print_id_thread_run);
            t3.Start();
            t4.Start();
            await t3;
            await t4;
            Console.WriteLine("end");
            await Task.Delay(100);

        }

         static void  change(  ref int a )
        {
             a++;
        } 

        static int print_bitches(string t)
        {
            Thread.Sleep(2000);
            Console.WriteLine(t);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            return 3;
        }
        public static async Task<int> standart_async(string s)
        {
            Task<int> out_to_console = new Task<int>(() => print_bitches(s));
            out_to_console.Start();

            return out_to_console.Result; 
        }

        public static async Task<int> print_two(string s)
        {

          int i = await   standart_async(s);
            return i;
        }
        //Основная фишка!!!!!!!!!!ConfigureAwait
        public static async  Task<int> test_changer()
        {  
           return  await Task.Run(() => {  Thread.Sleep(2000); Console.WriteLine("5"); return 5; }).ConfigureAwait(false);

        }

        public static async Task<int> list_changer()
        { //Идея создавать лист внутри асинхронного метода и сама главная функция должна быть асинхронной
          //  int i = await print_two("bithes");

            //int j = await print_two("bithes");
            //Console.WriteLine("\n " + i + j);
            List<int> s = new List<int>();
            s.Add(1);
            s.Add(2);
            s.Add(3);


            while (true)
            {
                
                Console.WriteLine(s[1]);

                var task_one = test_changer();
                var task_two = test_changer();

                
                Console.WriteLine(task_one.IsCompleted);
                await Task.Delay(3000);
                Console.WriteLine(task_one.IsCompleted);
                //Console.WriteLine(task.Status);
                await Task.Delay(3000);

                //Console.WriteLine(task.Status);

               // s[1] = await task_one;
                //s[2] = await task_two;
                //Console.WriteLine(task.Status);
            }
        

           return  1;
        }








        static void Main(string[] args)
        {
            //    Thread thread = Thread.CurrentThread;
            //Console.WriteLine("ID  главного потока " +  thread.ManagedThreadId);

            //  list_changer(); 
            //print_id_thread();
            // print();
            //print_id_thread_async();
            string test = Console.ReadLine();
            Console.Write(test);



            Console.ReadLine();
        }
    }
}
