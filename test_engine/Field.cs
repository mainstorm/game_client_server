﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_engine
{
    class Field
    {
         char[,] FIELD;
         int length = 20;  //x 
         int  width = 20;  //y
        public Field()
        {
            length = 20;
            width = 20;
            Init_field();
        }
        public Field(int x ,int y)
        {
            length = x;
             width = y;
            Init_field();
        }
        public void Init_field()
        {

            FIELD = new char[length , width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length ; j++)
                {
                    if (j == 0 || j == length  - 1 || i == 0 || i == width - 1)
                        FIELD[i, j] = '#';
                }
            }
        }
        public void Print_field()
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length ; j++)
                    Console.Write(FIELD[i, j]);
                Console.Write('\n');
            }
        }

        public void Clear_field()
        {
            for (int i = 1; i < width - 1; i++)
                for (int j = 1; j < length  - 1; j++)
                    FIELD[i, j] = ' ';
        }

        public void Add_player(Point point)
        {
            FIELD[point.y, point.x] = '*';
        }
    }
}
