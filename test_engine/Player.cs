﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_engine
{
    class Player
    {
        Point point;
        static Random r = new Random();
        public Player(Point point_player)
        {
            this.point = point_player;
        }
        public Player()
        {
            this.point = new Point();
        }
        public Point buttonUp()
        {
            point.y--;
            return point;
        }
        public Point buttonDown()
        {
            point.y++;
            return point;
        }
        public Point buttonLeft()
        {
            point.x--;
            return point;
        }
        public Point buttonRight()
        {
            point.x++;
            return point;
        }
        public Point RandomPoint()
        {
            
            point.x = r.Next(2, 19);
            point.y = r.Next(2, 19);
            return point;
        }
    }
}
